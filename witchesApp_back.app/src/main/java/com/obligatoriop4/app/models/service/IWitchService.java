package com.obligatoriop4.app.models.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import com.obligatoriop4.app.models.entity.Common;
import com.obligatoriop4.app.models.entity.Supreme;
import com.obligatoriop4.app.models.entity.Witch;

public interface IWitchService {

	Page<Witch> findAll(int page, int pageSize, Sort sort);
	
	Witch findById(int id);
	
	Supreme saveSupreme(Supreme witch);

	Common saveCommon(Common witch);
	
	void delete(int id);
}
