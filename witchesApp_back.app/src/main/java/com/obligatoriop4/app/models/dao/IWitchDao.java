package com.obligatoriop4.app.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.obligatoriop4.app.models.entity.Witch;

public interface IWitchDao extends PagingAndSortingRepository<Witch, Integer> {

}
