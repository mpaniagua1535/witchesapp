package com.obligatoriop4.app.models.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Special extends Spell {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int manifestYear;
	private String description;

	public int getManifestYear() {
		return manifestYear;
	}

	public void setManifestYear(int manifestYear) {
		this.manifestYear = manifestYear;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

}
