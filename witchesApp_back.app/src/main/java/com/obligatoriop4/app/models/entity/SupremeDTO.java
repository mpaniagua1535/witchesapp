package com.obligatoriop4.app.models.entity;

import java.util.Date;

public class SupremeDTO extends WitchDTO {
	private Date bornDate;

	public Date getBornDate() {
		return bornDate;
	}

	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}

}
