package com.obligatoriop4.app.models.entity;

public class CommonDTO extends WitchDTO {

	private String regionOrigin;
	private boolean flyBroom;
	private int supremeWitchId;
	
	public String getRegionOrigin() {
		return regionOrigin;
	}
	public void setRegionOrigin(String regionOrigin) {
		this.regionOrigin = regionOrigin;
	}
	
	public boolean getFlyBroom() {
		return flyBroom;
	}
	public void setFlyBroom(boolean flyBroom) {
		this.flyBroom = flyBroom;
	}
	
	public int getSupremeWitchId() {
		return supremeWitchId;
	}
	public void setSupremeWitchId(int supremeWitchId) {
		this.supremeWitchId = supremeWitchId;
	}

}
