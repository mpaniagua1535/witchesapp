import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { WitchDTO } from "../models/witchDTO";
import { WitchService } from "../service/witch.service";

@Component({
  selector: 'app-witch-detail',
  templateUrl: './witch-detail.component.html',
  styleUrls: ['./witch-detail.component.css']
})
export class WitchDetailComponent implements OnInit {

  witch: WitchDTO;

  constructor(private witchService: WitchService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.loadWitch();
  }

  loadWitch(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.witchService.getWitchById(id).subscribe((witch) => {
          this.witch = witch;
        })
      }
    })
  }

}
